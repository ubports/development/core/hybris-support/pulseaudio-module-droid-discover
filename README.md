pulseaudio-module-droid-discover
================================

This Pulseaudio module detects the base Android version, then load the
correct module for that version. It also detects the presence or absence
of AudioflingerGlue in the Android part, and load either -glue or -hidl
correctly.

This is useful if a distribution uses the "single rootfs" approach where
all applicable modules are installed at same time, as opposed to "per-
device rootfs" as used by e.g. SailfishOS.
